new Vue({

	// We want to target the div with an id of 'events'
	el: '#events',

	// Here we can register any values or collections that hold data
	// for the application
	data: {
		event: {name: '', description: '', date: '' },
		events: []
	},

	// Anything within the ready function will run when the application loads
	mounted: function() {
		// When the application loads, we want to call the method that initializes
		// some data
		this.fetchEvents();
	},

	// Methods we want to use in our application are registered here
	methods: {
		// We dedicate a method to retrieving and setting some data
		fetchEvents: function() {
			this.event = {
					id: 1,
					name: 'Test additional Event',
					description: 'Event added additionally in fetchEvents()',
					date: '2018-03-29'
				};
			this.events.push(this.event);
			// Vue.set is a convenience method provided by Vue that is similar to pushing data onto an array
			// Vue.set(this.events, 4, this.event);
			this.event = {name: '', description: '', date: ''};
			// // If we had a back end with an events endpoint set up that responds to GET requests
			// this.$http.get('api/events').then(function(events) {
			// 	Vue.set(this.events, this.events.length + 1, {
			// 		id: 1,
			// 		name: 'Test additional Event',
			// 		description: 'Event added additionally in fetchEvents()',
			// 		date: '2018-03-29'
			// 	});
			// });
		},

		// Adds an event to the existing events array
		addEvent: function() {
			if(this.event.name) {
				this.events.push(this.event);
				this.event = { name: '', description: '', date: '' };
			}
			// // If we had an endpoint set up that responds to POST requests
			// this.$http.post('api/events', this.event).then(function(response) {
			// 	this.events.push(this.event);
			// 	console.log("Event added!");
			// });
			// this.event = { name: '', description: '', date: '' };
		},

		// Remove from data array
		deleteEvent: function(index) {
			if (confirm("Are you sure you want to delete this event?")) {
				Vue.delete(this.events, index);
				//this.events.splice(index, 1);
			}
			// // We could also delete an event if we had the events endpoint set up to delete data
			// this.$http.delete('api/events/' + index).then(function(response) {
			// 	Vue.delete(this.events, index);
			// });
		}
	}
});